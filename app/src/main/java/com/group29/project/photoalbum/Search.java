package com.group29.project.photoalbum;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Search extends AppCompatActivity {
    protected Context context;
    protected MyAlbumList mal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*Display search results*/
        setContentView(R.layout.activity_search);
        context = this;
        String searchTerm = getIntent().getExtras().getString("query");
        execSearch(searchTerm);
        getActionBar().setTitle("Search Results | Value: " + searchTerm);
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void execSearch(String search){
        final List<Photo> result = new ArrayList<Photo>();
        search = search.toLowerCase();
        search.trim();
        final ArrayList<Album> albums = mal.getAlbums();
        for(Album a : albums){
            ArrayList<Photo> photos = a.getPhotos();
            for(Photo p : photos){
                for(Tag t : p.getTags()){
                    String temp = t.getValue();
                    temp.trim();
                    temp.toLowerCase();
                    if(temp.contains(search)){
                        if(!result.contains(p)){
                            result.add(p);
                        }
                    }
                }
            }
        }

        result.removeAll(Arrays.asList("", null));
        int size = result.size();
        String resStr = "";
        if(size <= 0){
            resStr = "No matching photos found for the search term.";
        }
        else{
            resStr = "Found matching photos!";
            ListView lv = (ListView) findViewById(R.id.list);
            lv.setAdapter(new ArrayAdapter<Photo>(this, R.layout.item_list_view, result));
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Photo photo = result.get(i);
                    //Intent intent = new Intent(context, FullImageDisplayActivity.class);
                    //startActivity(intent);
                }
            });
        }

    }


}