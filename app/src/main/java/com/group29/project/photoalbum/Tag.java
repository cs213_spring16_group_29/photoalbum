package com.group29.project.photoalbum;
import java.io.*;
import java.util.*;

/**
 * Created by ashnimehta on 5/4/16.
 */
public class Tag implements Serializable{
    private static final long serialVersionUID = 1L;
    String tt;
    String tv;

    public Tag(String tt, String tv){
        super();
        this.tt = tt;
        this.tv = tv;
    }

    public String getType(){
        return tt;
    }

    public String getValue(){
        return tv;
    }

    public void setType(String tt){
        this.tt = tt;
    }

    public void setValue(String tv){
        this.tv = tv;
    }

    public int hashCode(){
        int retVal = 1;
        final int pri = 47;

        retVal = pri * retVal + ((tt == null) ? 0 : tt.hashCode());
        retVal = pri * retVal + ((tv == null) ? 0 : tv.hashCode());
        return retVal;
    }

    public boolean equals(Object o){
        if(this == o){
            return true;
        }
        if(o == null){
            return false;
        }

        /*If object o is not a Tag*/
        if(! (o instanceof Tag)){
            return false;
        }
        Tag t = (Tag) o;

        if(tt == null && t.tt != null){
            return false;
        }
        if(tv == null && t.tv != null){
            return false;
        }
        if(!tt.equals(t.tt)){
            return false;
        }
        if(!tv.equals(t.tv)){
            return false;
        }

        return true;
    }






}
