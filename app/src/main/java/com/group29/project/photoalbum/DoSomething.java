package com.group29.project.photoalbum;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

/**
 * Created by Brandon on 5/4/2016.
 */
public class DoSomething extends AppCompatActivity {

    public static final String ALBUM_NAME = "albumName";
    public static final String ALBUM_ID = "albumID";

    EditText albumName;
    int albumID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.do_something);

        albumName = (EditText) findViewById(R.id.album_name);

        // check if Bundle was passed, and populate fields
        Bundle bundle = getIntent().getExtras();
        albumID = -1;
        if (bundle != null) {
            albumID = bundle.getInt(ALBUM_ID);
            albumName.setText(bundle.getString(ALBUM_NAME));
        }
    }

    public void callEdit(View view){
        /*They call edit*/
        String name = albumName.getText().toString();

        Bundle bundle = new Bundle();
        bundle.putString(ALBUM_NAME, name);
        bundle.putInt(ALBUM_ID, albumID);

        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
    }

    public void delete(View view){

    }
}