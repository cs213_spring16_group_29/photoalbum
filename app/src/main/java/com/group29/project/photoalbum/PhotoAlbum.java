package com.group29.project.photoalbum;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

public class PhotoAlbum extends AppCompatActivity {
    private MyAlbumList myList;
    private ListView listView;
    private static ArrayList<Album> currentAlbums;

    public static final int ADD_ALBUM_CODE=1;
    public static final int EDIT_ALBUM_CODE=2;
    public static final int ASK_EDIT_CODE = 3;

    public void addAlbum(View view) {
        Intent intent = new Intent(this, AddAlbum.class);
        startActivityForResult(intent, ADD_ALBUM_CODE);
    }

    public void showAlbum(Bundle b){
        Intent intent = new Intent(this, AddAlbum.class);
        intent.putExtras(b);
        startActivityForResult(intent, EDIT_ALBUM_CODE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_album);

        try{
            // get instance of MyAlbumList
            myList = MyAlbumList.getInstance(this);
           /* myList.setContext(this);
            myList.load();*/
        }catch (IOException e){
            Toast.makeText(this, "Error loading songs", Toast.LENGTH_LONG).show();
        }

        // load initial set of albums if there is no data file to load from
        if(myList == null){
            String[] initAlbums = getResources().getStringArray(R.array.album_array);
            //grab all the sample images
            TypedArray imgs = getResources().obtainTypedArray(R.array.image_ids);
            ArrayList<Photo> temp = new ArrayList<Photo>();
            for (int i = 0; i < imgs.length(); i++) {
                temp.add(new Photo(imgs.getResourceId(i, -1)));
            }
            // break albums into name and artist, add to list
            for (String album: initAlbums) {
                /*only necessary for special name formatting like if
                    we include the last edited date or something
                int pos = album.indexOf('|');
                myList.add(album.substring(0,pos));*/
                myList.add(album, temp);
            }
        }

        //At this point, we are ready to output the list
        // get ListView object
        listView = (ListView)findViewById(R.id.album_list);
        // fit listView with adapter off of myList, and album layout
        listView.setAdapter(
                new ArrayAdapter<Album>(this,R.layout.album, myList.getAlbums()));
        //ugh
        currentAlbums = myList.getAlbums();
        // listen to item click
        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent = new Intent (PhotoAlbum.this, PhotoGridView.class);

                        Album albumToCheck = null;
                        if(myList.getAlbums() != null)
                            albumToCheck = myList.getAlbums().get(position);
                        ArrayList<Photo> quick = null;
                        if(albumToCheck != null){
                            quick = albumToCheck.getPhotos();
                        }
                        ArrayList<Integer> photoKeys = null;
                        if(quick != null && (!quick.isEmpty())){
                            photoKeys = new ArrayList<Integer>();
                            for(Photo z: quick){
                                photoKeys.add(z.path);
                            }
                        }
                        if(photoKeys != null){
                            Bundle bundle = new Bundle();
                            bundle.putIntegerArrayList("PHOTO_LIST",photoKeys);
                            intent.putExtras(bundle);
                        }

                        startActivity(intent);
                    }
                }
        );
        listView.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent = new Intent (PhotoAlbum.this, DoSomething.class);
                        startActivity(intent);
                        return true;
                        /*DialogFragment newFragment = new AlbumActionsDialogFragment();
                        newFragment.show(getFragmentManager(), "albumInfoError");
                        return true;*/
                    }
                }
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        //maybe changing the value of RESULT_OK would help in the future?
        if (resultCode != RESULT_OK) { return; }
        Bundle bundle = intent.getExtras();
        if (bundle == null) { return; }
        String name = bundle.getString(AddAlbum.ALBUM_NAME);
        int id = bundle.getInt(AddAlbum.ALBUM_ID);

        //Make this DRY another day.
        if (requestCode == ADD_ALBUM_CODE) {
            myList.add(name);
            listView.setAdapter(
                    new ArrayAdapter<Album>(this,
                            R.layout.album, myList.getAlbums()));
        }
        else if(requestCode == EDIT_ALBUM_CODE) {
            Album album = new Album(id, name);
            myList.update(album);
            listView.setAdapter(
                    new ArrayAdapter<Album>(this,
                            R.layout.album, myList.getAlbums()));
        }
        else if(requestCode == ASK_EDIT_CODE){
            showAlbum(bundle);
        }


        //ugh
        currentAlbums = myList.getAlbums();
    }


    public void launchSearch(View view){
        //fill in
    }

    public static ArrayList<Album> getCurrentList(){
        return currentAlbums;
    }
}
