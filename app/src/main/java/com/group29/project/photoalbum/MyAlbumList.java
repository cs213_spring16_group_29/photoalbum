package com.group29.project.photoalbum;

/**
 * Created by Brandon on 5/1/2016.
 */

import android.content.Context;
import android.widget.Toast;

import java.io.*;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class MyAlbumList implements AlbumList {

    // single instance
    private static MyAlbumList albumList=null;

    // holds albums in a sorted array list
    private ArrayList<Album> albums;

    // keep track of max id dealt out so far
    private int maxId;

    Context context;
    public static final String ALBUMS_FILE = "albums.dat";

    // make constructor private for single instance control
    private MyAlbumList() {
        albums = new ArrayList<Album>();
        maxId = -1;
    }

    // deal out the singleton
    public static MyAlbumList getInstance(Context ctx) throws IOException{
        if (albumList == null) {
            albumList = new MyAlbumList();
            albumList.context = ctx;
            albumList.load();
        }
        return albumList;
    }

    public Album add(String name) {
        // name and artist are mandatory
        if (name == null) {
            throw new IllegalArgumentException("Name is mandatory!");
        }

        // set id to next available
        maxId++;

        // create Album object
        Album album = new Album(maxId, name);

        // if this is the first add, it's easy
        if (albums.size() == 0) {
            albums.add(album);
            try{
                store();
            } catch(IOException e){
                Toast.makeText(context, "Could not store albums to file",
                        Toast.LENGTH_LONG).show();
            }

            return album;
        }

        // search in array list and add at correct spot
        int lo=0, hi=albums.size()-1, mid=-1, c=0;
        while (lo <= hi) {
            mid = (lo+hi)/2;
            c = album.compareTo(albums.get(mid));
            if (c == 0) {  // duplicate name
                break;
            }
            if (c < 0) {
                hi = mid-1;
            } else {
                lo = mid+1;
            }
        }
        int pos = c <= 0 ? mid : mid+1;
        // insert at pos
        albums.add(pos,album);
        // write through
        try {
            store();
            return album;
        } catch (IOException e) {
            return null;
        }

    }

    public Album add(String name, ArrayList<Photo> photos) {
        // name and artist are mandatory
        if (name == null) {
            throw new IllegalArgumentException("Name is mandatory!");
        }

        // set id to next available
        maxId++;

        // create Album object
        Album album = new Album(maxId, name);
        for(Photo item: photos){
            //add a little randomness for fun!
            double guess = Math.random();
            if (guess % 2 == 0)
                album.addPhoto(item);
        }

        // if this is the first add, it's easy
        if (albums.size() == 0) {
            albums.add(album);
            try{
                store();
            } catch(IOException e){
                Toast.makeText(context, "Could not store albums to file",
                        Toast.LENGTH_LONG).show();
            }

            return album;
        }

        // search in array list and add at correct spot
        int lo=0, hi=albums.size()-1, mid=-1, c=0;
        while (lo <= hi) {
            mid = (lo+hi)/2;
            c = album.compareTo(albums.get(mid));
            if (c == 0) {  // duplicate name
                break;
            }
            if (c < 0) {
                hi = mid-1;
            } else {
                lo = mid+1;
            }
        }
        int pos = c <= 0 ? mid : mid+1;
        // insert at pos
        albums.add(pos,album);
        // write through
        try {
            store();
            return album;
        } catch (IOException e) {
            return null;
        }

    }

    public int getPos(Album album) {
        if (albums.size() == 0) {
            return -1;
        }

        // search in array list for name match, then id match
        int lo=0, hi=albums.size()-1;

        while (lo <= hi) {
            int mid = (lo+hi)/2;
            Album lalbum = albums.get(mid);
            int c = album.compareTo(lalbum);
            if (c == 0) {  // need to compare id
                if (album.id == lalbum.id) {
                    return mid;
                }
                // check left
                int i=mid-1;
                while (i >= 0) {
                    lalbum = albums.get(i);
                    if (album.compareTo(lalbum) == 0 && album.id == lalbum.id) {
                        return i;
                    }
                    i--;
                }
                // check right
                i = mid+1;
                while (i < albums.size()) {
                    lalbum = albums.get(i);
                    if (album.compareTo(lalbum) == 0 && album.id == lalbum.id) {
                        return i;
                    }
                    i++;
                }
                return -1;
            }
            if (c < 0) {
                hi = mid-1;
            } else {
                lo = mid+1;
            }
        }
        return -1;
    }

    public ArrayList<Album> getAlbums() {
        return albums;
    }

    public void load() throws IOException {
        int maxId=-1;
        try {
            FileInputStream fis =
                    context.openFileInput(ALBUMS_FILE);
            BufferedReader br =
                    new BufferedReader(
                            new InputStreamReader(fis));
            String albumName;
            while ((albumName = br.readLine()) != null) {
                Album album = new Album(0, albumName);
                maxId++;
                album.id = maxId;
                albums.add(album);
            }
            this.maxId = maxId;
            fis.close();

        } catch (FileNotFoundException e) {  // default to initial set
            // load initial set of songs
            String[] initAlbums = context.getResources()
                    .getStringArray(R.array.album_array);
            // break songs into name and artist, add to list
            for (String albumStr: initAlbums) {
                //int pos = album.indexOf('|');
                add(albumStr);
            }

        }
    }

    public ArrayList<Album> remove(Album album) throws NoSuchElementException {
        int pos = getPos(album);
        if (pos == -1) {
            throw new NoSuchElementException();
        }
        albums.remove(pos);
        try {
            store();
        } catch (IOException e) {
            Toast.makeText(context,"Could not store albums to file",
                    Toast.LENGTH_LONG).show();
        }
        return albums;
    }

    public void setContext(Context ctx) {
        context = ctx;
    }

    public void store() throws IOException {
        FileOutputStream fos =
                context.openFileOutput(ALBUMS_FILE,Context.MODE_PRIVATE);
        PrintWriter pw = new PrintWriter(fos);
        for (Album album: albums) {
            pw.println(album.toString());
        }
        pw.close();
    }

    public ArrayList<Album> update(Album album) throws NoSuchElementException {
        // since name could be updated, best to sequentially search on id
        for (int i=0; i < albums.size(); i++) {
            if (albums.get(i).id == album.id) {
                albums.set(i, album);
                try {
                    store();
                } catch (IOException e) {
                    Toast.makeText(context,"Could not store albums to file",
                            Toast.LENGTH_LONG).show();
                }
                return albums;
            }
        }
        throw new NoSuchElementException();
    }

}
