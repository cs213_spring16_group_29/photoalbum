package com.group29.project.photoalbum;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.GridView;

import java.util.ArrayList;

/**
 * Created by ashnimehta on 5/2/16.
 */

public class PhotoGridView extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gridview_main);
        GridView gridview = (GridView) findViewById(R.id.gridview);

        // tried this earlier: savedInstanceState.getIntegerArrayList("PHOTO_LIST");
        if(getIntent().getExtras() == null){
            if(gridview.getAdapter() == null) {
                TypedArray imgs = getResources().obtainTypedArray(R.array.image_ids);
                ArrayList<Integer> whoop = new ArrayList<Integer>();
                for (int i = 0; i < imgs.length(); i++) {
                    whoop.add(imgs.getResourceId(i, -1));
                }
                gridview.setAdapter(new ImageAdapter(this, whoop));
            }
        }
        else{
            ArrayList<Integer> temp = getIntent().getExtras().getIntegerArrayList("PHOTO_LIST");
            if (temp != null) {
                if(gridview.getAdapter() == null){
                /*TypedArray imgs = getResources().obtainTypedArray(R.array.image_ids);
                ArrayList<Integer> temp = new ArrayList<Integer>();
                for (int i = 0; i < imgs.length(); i++) {
                    temp.add(imgs.getResourceId(i, -1));
                }*/
                    gridview.setAdapter(new ImageAdapter(this, temp));
                }
            }
            else{
                setContentView(R.layout.add_album);
            }

        }


        /*gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Toast.makeText(HelloGridView.this, "" + position,
                Toast.LENGTH_SHORT).show();
            }
        });*/

    }
}
