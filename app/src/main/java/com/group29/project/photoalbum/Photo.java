package com.group29.project.photoalbum;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ashnimehta on 5/4/16.
 */
public class Photo {
    private static final long serialVersionUID = 1L;
    String album;
    Integer path;
    List<Tag> tags;

    public Photo(Integer path, List<Tag> tags){
        super();
        this.path = path;
        this.tags = tags;
    }

    public Photo(Integer path){
        super();
        this.path = path;
        this.tags = new ArrayList<Tag>();
    }

    public Photo(Integer path, List<Tag> tags, String album){
        this.path = path;
        this.tags = tags;
        this.album = album;
    }

    public String getAlbum(){
        return this.album;
    }

    public List<Tag> getTags(){
        return this.tags;
    }

    public void setAlbum(String s){
        this.album = s;
    }

    public void setPath(Integer s){
        this.path = s;
    }

    public void setTags(List<Tag> tags){
        this.tags = tags;
    }

    public boolean addTag(Tag t){
        if(this.tags.contains(t)){
            return false;
        }
        else{
            tags.add(t);
            return true;
        }
    }

    public boolean addTag(String tt, String tv){
        Tag t = new Tag(tt, tv);
        if(this.tags.contains(t)){
            return false;
        }
        else{
            tags.add(t);
            return true;
        }
    }

    public boolean deleteTag(String tt, String tv){
        Tag t = new Tag(tt, tv);
        if(this.tags.contains(t)){
            this.tags.remove(t);
            return true;
        }
        else return false;
    }

    public boolean equals(Object o){
        if(o == null){
            return false;
        }
        if(this == o) {
            return true;
        }
        if(o.getClass() != getClass()){
            return false;
        }

        /*Now we can try casting the object to a Photo*/
        Photo p = (Photo) o;

        //check path
        if(path == null && p.path != null){
            return false;
        }
        else if (path != p.path){
            return false;
        }

        //check album
        if(album == null && p.album != null){
            return false;
        }
        else if(!album.equals(p.album)){
            return false;
        }

        //check tags
        if(tags == null && p.tags != null){
            return false;
        }

        else if(!tags.equals(p.tags)){
            return false;
        }

        /*What else is there*/
        return true;
    }

    public int hashCode(){
        int retVal = 1;
        final int pri = 47;

        retVal = pri * retVal + ((path == null) ? 0 : path.hashCode());
        retVal = pri * retVal + ((album == null) ? 0 : album.hashCode());
        retVal = pri * retVal + ((tags == null) ? 0 : tags.hashCode());
        return retVal;
    }

}
