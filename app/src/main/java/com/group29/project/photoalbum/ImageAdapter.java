package com.group29.project.photoalbum;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by ashnimehta on 5/2/16.
 */
public class ImageAdapter extends BaseAdapter {
    private Context mContext;

    // references to our images
    //I think this will be the key to our dynamic loading of images.
    //It may not need to be an arraylist if we can access the list of image resources
    //  and know the size of that list at runtime.
    private ArrayList<Integer> mThumbsIds;

    public ImageAdapter(Context c, ArrayList<Integer> resources) {
        mContext = c;
        mThumbsIds = new ArrayList<Integer>();
        mThumbsIds.add(R.drawable.sample_7);
        mThumbsIds.add(R.drawable.sample_0);
        mThumbsIds.add(R.drawable.sample_1);
        mThumbsIds.add(R.drawable.sample_5);
        mThumbsIds.add(R.drawable.sample_3);
        mThumbsIds.addAll(resources);
    }

    public int getCount() {
        return mThumbsIds.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            //sets the image width and height respectively
            imageView.setLayoutParams(new GridView.LayoutParams(160, 160));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageResource(mThumbsIds.get(position));
        return imageView;
    }

}
