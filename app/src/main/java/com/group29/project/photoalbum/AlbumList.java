package com.group29.project.photoalbum;

/**
 * Created by Brandon on 5/1/2016.
 */
import android.content.Context;

import java.io.IOException;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public interface AlbumList {
    void setContext(Context ctx);
    void load() throws IOException;
    void store() throws IOException;
    ArrayList<Album> getAlbums();
    Album add(String name);
    ArrayList<Album> update(Album Album) throws NoSuchElementException;
    ArrayList<Album> remove(Album Album) throws NoSuchElementException;
    int getPos(Album Album);
}

