package com.group29.project.photoalbum;

import java.io.*;
import java.util.*;

/**
 * Created by Brandon on 5/1/2016.
 */

public class Album implements Serializable{
    private static final long serialVersionUID = 1L;

    public int id;
    public String name;
    public ArrayList<Photo> photos;

    public Album(int id, String name) {
        this.id = id;
        this.name = name;
        this.photos = new ArrayList<Photo>();
    }

    public int compareTo(Album other) {

        return name.compareToIgnoreCase(other.name);
    }

    public String getName(){
        return this.name;
    }

    public void setAlbumName(String newName){
        this.name = newName;
    }

    public ArrayList<Photo> getPhotos(){
        return this.photos;
    }

    public void setPhotos(ArrayList<Photo> photos){
        this.photos = photos;
    }

    //will first photo be any different?

    public boolean addPhoto(Photo p){
        if(photos.contains(p)){
            return false;
        }
        else{
            //p.setAlbum(path);
            /*Hash based on path and photo*/
            this.photos.add(p);
            return true;
        }
    }

    public boolean deletePhoto(Photo p){
        if(photos.contains(p)){
            photos.remove(p);
            return true;
        }
        else
        {
            return false;
        }
    }

    public String toString() {   // this is for ListView adapter

        return name;
        //This could be a fun way to add text that indicates the last date of editing:
        // + "\n(" + date_last_edited + ")";
    }

    public String getString() {

        return name + ":" + id;
    }

    /*How do you build a hashing function??????????*/
    /*Doing the basic hashing function?? maybe this'll work*/

    public int hashCode(){
        final int pri = 47;
        int retVal = 1;
        retVal = pri * retVal + ((name == null) ? 0 : name.hashCode());
        retVal = pri * retVal + ((photos == null) ? 0 : photos.hashCode());
        return retVal;
    }
}