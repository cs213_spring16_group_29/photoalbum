package com.group29.project.photoalbum;

/**
 * Created by Brandon on 5/2/2016.
 */
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;

public class AddAlbum extends AppCompatActivity {

    public static final String ALBUM_NAME = "albumName";
    public static final String ALBUM_ID = "albumID";

    EditText albumName;
    int albumID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_album);

        albumName = (EditText)findViewById(R.id.album_name);

        // check if Bundle was passed, and populate fields
        Bundle bundle = getIntent().getExtras();
        albumID = -1;
        if (bundle != null) {
            albumID = bundle.getInt(ALBUM_ID);
            albumName.setText(bundle.getString(ALBUM_NAME));
        }
    }

    // called when the user taps the Save button
    public void save(View view) {
        // gather all data
        String name = albumName.getText().toString();

        // name is mandatory
        if (name == null || name.length() == 0) {
            DialogFragment newFragment = new AlbumInfoDialogFragment();
            newFragment.show(getFragmentManager(), "albumInfoError");

            return;   // does not quit activity, just returns from method
        }

        //This makes a check to see if that album name already exists.
        ArrayList<Album> currentList = PhotoAlbum.getCurrentList();
        for(Album someAlbum: currentList){
            if(name.equals(someAlbum.name)){
                DialogFragment newFragment = new AlbumInfoDialogFragment();
                newFragment.show(getFragmentManager(), "albumInfoError");
                return;   // does not quit activity, just returns from method
            }
        }


        // make Bundle
        Bundle bundle = new Bundle();
        bundle.putString(ALBUM_NAME,name);
        bundle.putInt(ALBUM_ID, albumID);

        // send back to caller
        Intent intent = new Intent();
        intent.putExtras(bundle);

        setResult(RESULT_OK,intent);
        finish(); // pops the activity from the call stack, returns to parent
    }

    // called when the user taps the Cancel button
    public void cancel(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }

}
